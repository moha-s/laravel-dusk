<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'my_project');
set('ssh_multiplexing', true); // Speed up deployment

// Project repository
set('repository', 'https://gitlab.com/moha-s/laravel-dusk');

// Hosts

host('test.mohamed-salih.xyz')
    ->hostname('31.220.53.180')
    ->user('root')
    ->set('deploy_path', '/home/test.mohamed-salih.xyz/public_html');
    
// Tasks
task('clone', function () {
    run('cd {{deploy_path}} && git clone {{repository}} .');
});

task('install', function () {
    run('cd {{deploy_path}} && composer update --ignore-platform-reqs');
});

task('site:up', function () {
    writeln(sprintf('  <info>%s</info>', run('php {{deploy_path}}/artisan up')));
});

desc('Deploy the application');
task('deploy', [
    'clone',
    'install',
    'site:up',
    'cleanup',
    'success'
]);