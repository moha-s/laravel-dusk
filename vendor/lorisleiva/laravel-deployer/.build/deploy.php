<?php

namespace Deployer;

require 'recipe/laravel-deployer.php';

/*
 * Includes
 */

/*
 * Options
 */

set('strategy', 'basic');
set('application', 'Laravel');
set('repository', 'git@gitlab.com:moha-s/laravel-dusk.git');

/*
 * Hosts and localhost
 */

host('1999.esy.es')
    ->set('deploy_path', '/home/1999.esy.es/public_html')
    ->user('root');

/*
 * Strategies
 */

/*
 * Hooks
 */

after('hook:ready', 'artisan:storage:link');
after('hook:ready', 'artisan:view:clear');
after('hook:ready', 'artisan:config:cache');
after('hook:ready', 'artisan:migrate');